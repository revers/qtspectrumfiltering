/*
 * RevIDSP.h
 *
 *  Created on: 12-10-2012
 *      Author: Revers
 */

#ifndef REVIDSP_HPP_
#define REVIDSP_HPP_

namespace rev {
    class IDSP {
    public:
        virtual ~IDSP() {
        }

        virtual void process(float* leftData, float* rightData,
                int dataSize) = 0;
    };
}

#endif /* REVIDSP_HPP_ */
