/*
 * SignalSpectrumGraph.h
 *
 *  Created on: 17-10-2012
 *      Author: Revers
 */

#ifndef SIGNALSPECTRUMGRAPH_H_
#define SIGNALSPECTRUMGRAPH_H_

#include <QWidget>
#include <QSize>
#include <QColor>

#include "../RevIDSP.h"

class QImage;

//namespace boost {
//    class mutex;
//}

namespace rev {

    /**
     * This scaling methods are from
     * http://inchoatethoughts.com/a-wpf-spectrum-analyzer-for-audio-visualization-c-part-2-the-wpfening
     */
    enum class ScalingType {
        /*
         * A decibel scale. Formula: 20 * Log10(FFTValue). Total bar height
         * is scaled from -90 to 0 dB.
         */
        DECIBEL,

        /*
         * A non-linear squareroot scale. Formula:
         * Sqrt(FFTValue) * 2 * BarHeight.
         */
        SQRT,

        /*
         * A linear scale. Formula: 9 * FFTValue * BarHeight.
         */
        LINEAR
    };

    class SignalSpectrumGraph: public QWidget, public IDSP {
    Q_OBJECT

        QColor bgColor;
        QImage* imageBack = nullptr;
        QImage* imageFront = nullptr;
        int imageWidth = 0;
        int imageHeight = 0;

        const int scaleFactorLinear = 9;
        const int scaleFactorSqr = 2;
        const float minDBValue = -90;
        const float maxDBValue = 0;

        /**
         * Array of size 2 * fftDataSize.
         */
        float* fftData = nullptr;
        float* localFftData = nullptr;
        float* windowData = nullptr;
        float* barsBuffer = nullptr;

        float* leftData = nullptr;
        float* rightData = nullptr;
        bool buffersInited = false;

        /**
         * Amount of complex samples.
         */
        int fftDataSize = 0;

        ScalingType scalingType = ScalingType::DECIBEL;

        int bars = 0;
        int barWidth = 0;

        int minFrequency = 0;
        int maxFrequency = 0;
        int sampleRate = 0;

        float gain = 1.0f;
        bool canEmitRepaint = true;

      //  boost::mutex* freqMutex = nullptr;
     //   boost::mutex* canEmitRepaintMutex = nullptr;

    public:

        SignalSpectrumGraph(QWidget* parent = 0);

        virtual ~SignalSpectrumGraph();

        void process(float* leftData, float* rightData,
                int dataSize) override;

        int getMaxFrequency() const {
            return maxFrequency;
        }

        int getMinFrequency() const {
            return minFrequency;
        }

//        void setMaxFrequency(int maxFrequency);
//        void setMinFrequency(int minFrequency);

        void setMaxFrequency(int maxFrequency) {
            this->maxFrequency = maxFrequency;
        }

        void setMinFrequency(int minFrequency) {
            this->minFrequency = minFrequency;
        }

        int getSampleRate() const {
            return sampleRate;
        }

        void setSampleRate(int sampleRate) {
            this->sampleRate = sampleRate;
        }

        float getGain() const {
            return gain;
        }

        void setGain(float gain) {
            this->gain = gain;
        }

        ScalingType getScalingType() const {
            return scalingType;
        }

        void setScalingType(ScalingType scalingType) {
            this->scalingType = scalingType;
        }

        void drawBackImage();

    protected:

        void paintEvent(QPaintEvent* event) override;

        virtual QSize sizeHint() const override {
            return QSize(300, 100);
        }
    signals:
        void queuedRepaint();

    public slots:
        void requestRepaint();

    private:
        void margeAndWindowData();
        void fillWindowData();
        void initBuffers();
        void processSignal();
    };

} /* namespace rev */

#endif /* SIGNALSPECTRUMGRAPH_H_ */
