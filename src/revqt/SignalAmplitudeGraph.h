/*
 * SignalAmplitudeGraph.h
 *
 *  Created on: 16-10-2012
 *      Author: Revers
 */

#ifndef SIGNALAMPLITUDEGRAPH_H_
#define SIGNALAMPLITUDEGRAPH_H_

#include <QWidget>
#include <QSize>
#include <QColor>

#include "../RevIDSP.h"

class QImage;

namespace rev {

    class SignalAmplitudeGraph: public QWidget, public IDSP {
    Q_OBJECT

        QColor bgColor;
        QImage* imageBack = nullptr;
        QImage* imageFront = nullptr;
        float* leftData = nullptr;
        float* rightData = nullptr;
        int imageWidth = 0;
        int imageHeight = 0;
        int dataSize = 0;

        bool canEmitRepaint = true;

    public:

        SignalAmplitudeGraph(QWidget* parent = 0);

        virtual ~SignalAmplitudeGraph();

        void process(float* leftData, float* rightData,
                int dataSize) override;

    protected:

        void paintEvent(QPaintEvent* event) override;

        virtual QSize sizeHint() const override {
            return QSize(300, 100);
        }

    public slots:
        void requestRepaint();

    signals:
        void queuedRepaint();

    private:

        void drawBackImage();
    };

} /* namespace rev */
#endif /* SIGNALAMPLITUDEGRAPH_H_ */
