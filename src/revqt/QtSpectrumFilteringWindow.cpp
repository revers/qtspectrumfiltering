/*
 * File:   QtSpectrumFilteringWindow.cpp
 * Author: Kamil
 *
 * Created on 13 czerwiec 2012, 13:32
 */

#include <iostream>

#include <QCloseEvent>
#include <QButtonGroup>
#include <QSizePolicy>
#include <QEvent>
#include <QDebug>
#include <QString>
#include <QFile>
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>
#include <QMouseEvent>
#include <QSettings>
#include <QStackedWidget>

#include <rev/common/RevAssert.h>
#include <rev/common/RevSleep.h>

#include "QtSpectrumFilteringWindow.h"
#include "SignalAmplitudeGraph.h"
#include "SignalSpectrumGraph.h"

using namespace std;

#define HOUR (1000 * 60 * 60)
#define MINUTE (1000 * 60)
#define SECOND 1000

#define FRAMES_AT_ONCE 2048

QtSpectrumFilteringWindow::QtSpectrumFilteringWindow(
        rev::IAudioOutputStreamPtr audioOutputPtr) :
        player(audioOutputPtr) {

    widget.setupUi(this);

    signalAmplitudeGraph = new rev::SignalAmplitudeGraph(this);
    signalSpectrumGraph = new rev::SignalSpectrumGraph(this);

    widget.visualizationsLayout->addWidget(signalAmplitudeGraph);
    widget.visualizationsLayout->addWidget(signalSpectrumGraph);

    player.addDSP(signalSpectrumGraph);
    player.addDSP(signalAmplitudeGraph);

    widget.visualizationsLayout->setStretchFactor(signalAmplitudeGraph, 666666);
    signalAmplitudeGraph->setSizePolicy(QSizePolicy::Expanding,
            QSizePolicy::Expanding);
    widget.visualizationsLayout->setStretchFactor(signalSpectrumGraph, 444444);
    signalSpectrumGraph->setSizePolicy(QSizePolicy::Expanding,
            QSizePolicy::Expanding);

    QButtonGroup* scaleGroup = new QButtonGroup(this);
    scaleGroup->addButton(widget.scaleDecibelRB);
    scaleGroup->addButton(widget.scaleLinearRB);
    scaleGroup->addButton(widget.scaleSqrtRB);

    player.setListener(this);

    widget.playButton->setDisabled(true);
    widget.pauseButton->setDisabled(true);
    widget.stopButton->setDisabled(true);

    widget.progressSlider->installEventFilter(this);

    setupActions();

    resize(1044, 820);
}

QtSpectrumFilteringWindow::~QtSpectrumFilteringWindow() {
}

void QtSpectrumFilteringWindow::scaleButtonChecked(bool checked) {

    if (widget.scaleDecibelRB->isChecked()) {
        signalSpectrumGraph->setScalingType(rev::ScalingType::DECIBEL);
    } else if (widget.scaleLinearRB->isChecked()) {
        signalSpectrumGraph->setScalingType(rev::ScalingType::LINEAR);
    } else if (widget.scaleSqrtRB->isChecked()) {
        signalSpectrumGraph->setScalingType(rev::ScalingType::SQRT);
    }
}

void QtSpectrumFilteringWindow::setupActions() {

    connect(widget.actionExit, SIGNAL(triggered(bool)), qApp, SLOT(quit()));

    connect(widget.actionOpen, SIGNAL(triggered(bool)),
            this, SLOT(openAction()));

    connect(widget.openButton, SIGNAL(clicked()),
            this, SLOT(openAction()));

    connect(widget.actionAbout, SIGNAL(triggered(bool)),
            this, SLOT(aboutAction()));

    connect(widget.scaleDecibelRB, SIGNAL(toggled(bool)),
            this, SLOT(scaleButtonChecked(bool)));

    connect(widget.scaleLinearRB, SIGNAL(toggled(bool)),
            this, SLOT(scaleButtonChecked(bool)));

    connect(widget.scaleSqrtRB, SIGNAL(toggled(bool)),
            this, SLOT(scaleButtonChecked(bool)));

    connect(widget.playButton, SIGNAL(clicked()),
            this, SLOT(playAction()));

    connect(widget.pauseButton, SIGNAL(clicked()),
            this, SLOT(pauseAction()));

    connect(widget.stopButton, SIGNAL(clicked()),
            this, SLOT(stopAction()));

    connect(widget.progressSlider, SIGNAL(valueChanged(int)),
            this, SLOT(progressSliderValueChangedEvent(int)), Qt::QueuedConnection);

    connect(widget.minHzSlider, SIGNAL(valueChanged(int)),
            this, SLOT(minimalFrequencyValueChanged(int)), Qt::QueuedConnection);

    connect(widget.maxHzSlider, SIGNAL(valueChanged(int)),
            this, SLOT(maximalFrequencyValueChanged(int)), Qt::QueuedConnection);

    connect(widget.gainSlider, SIGNAL(valueChanged(int)),
            this, SLOT(gainValueChanged(int)), Qt::QueuedConnection);

    connect(this, SIGNAL(stopSignal()), this, SLOT(stopSlot()), Qt::QueuedConnection);

    connect(this, SIGNAL(pauseSignal()), this, SLOT(pauseSlot()), Qt::QueuedConnection);

    connect(this, SIGNAL(playSignal()), this, SLOT(playSlot()), Qt::QueuedConnection);

    connect(this, SIGNAL(timeSignal(unsigned int)), this, SLOT(timeSlot(unsigned int)), Qt::QueuedConnection);

    connect(this, SIGNAL(repaintGraphsSignal()), signalSpectrumGraph, SLOT(requestRepaint()), Qt::QueuedConnection);
    connect(this, SIGNAL(repaintGraphsSignal()), signalAmplitudeGraph, SLOT(requestRepaint()), Qt::QueuedConnection);
}

void QtSpectrumFilteringWindow::closeEvent(QCloseEvent* event) {

    player.stop();
    REV_TRACE_MSG("Stopping player...");

    do {
        rev::sleep(100);
    } while (!player.isStopped());
}

bool QtSpectrumFilteringWindow::eventFilter(QObject* watched, QEvent* e) {

    if (e->type() == QEvent::MouseButtonPress) {
        QMouseEvent* event = static_cast<QMouseEvent*>(e);
        progressSliderMousePressedEvent(event);
    } else if (e->type() == QEvent::MouseButtonRelease) {
        QMouseEvent* event = static_cast<QMouseEvent*>(e);
        progressSliderMouseReleasedEvent(event);
    }

    return QWidget::eventFilter(watched, e);
}

void QtSpectrumFilteringWindow::aboutAction() {
    QMessageBox::information(this, "About...", "Author: Kamil Kolaczynski");
}

void QtSpectrumFilteringWindow::openAction() {

    const QString DEFAULT_DIR_KEY("default_dir");

    QSettings mySettings;

    QString selfilter = tr("MP3 (*.mp3)");
    QString filename = QFileDialog::getOpenFileName(
            this,
            tr("Open file"),
            mySettings.value(DEFAULT_DIR_KEY).toString(),
            tr("MP3 (*.mp3)"),
            &selfilter);

    if (filename.isEmpty()) {
        return;
    }

    player.stop();

    while (!player.isStopped()) {
        rev::sleep(100);
    }

    QDir currentDir;
    mySettings.setValue(DEFAULT_DIR_KEY, currentDir.absoluteFilePath(filename));

    QFileInfo info(filename);
    REV_TRACE_MSG("filename = " << info.fileName().data() << "...");

    if (!player.open(filename.toAscii().data(), FRAMES_AT_ONCE)) {
        widget.titleL->setText(QString("ERROR: Cannot open: ") + info.fileName());

        widget.playButton->setDisabled(true);
        widget.pauseButton->setDisabled(true);
        widget.stopButton->setDisabled(true);
        return;
    }

    widget.titleL->setText(info.fileName());
    int ms = player.getTrackLenghtMs();
    msPerSample = player.getMsPerSample();

    if (ms / HOUR > 0) {
        gotHours = true;
    } else {
        gotHours = false;
    }

    if (ms / MINUTE > 0) {
        gotMinutes = true;
    } else {
        gotMinutes = false;
    }

    QString time = msToString(ms);
    widget.totalTimeL->setText(time);
    widget.progressSlider->setMinimum(0);
    widget.progressSlider->setValue(0);
    widget.progressSlider->setMaximum(player.getTrackLengthSamples() - 1);

    rev::AudioFormat format = player.getFormat();
    widget.channelsL->setText(QString::number(format.channels));
    widget.framesAtOnceL->setText(QString::number(player.getFramesAtOnce()));
    widget.sampleRateL->setText(QString::number(format.sampleRate));

    int maxFreq = format.sampleRate / 2;
    widget.hzMaxL->setText(QString::number(maxFreq));
    widget.hzMidL->setText(QString::number(maxFreq / 2));

    widget.minHzSlider->setMinimum(0);
    widget.minHzSlider->setMaximum(maxFreq);
    widget.minHzSlider->setValue(1);
    // trigger value changed:
    widget.minHzSlider->setValue(0);

    widget.maxHzSlider->setMinimum(0);
    widget.maxHzSlider->setMaximum(maxFreq);
    widget.maxHzSlider->setValue(maxFreq - 1);
    // trigger value changed:
    widget.maxHzSlider->setValue(maxFreq);

    signalSpectrumGraph->setMinFrequency(widget.minHzSlider->value());
    signalSpectrumGraph->setMaxFrequency(widget.maxHzSlider->value());
    signalSpectrumGraph->setSampleRate(format.sampleRate);

    //signalSpectrumGraph->resetRepaint();
}

void QtSpectrumFilteringWindow::playAction() {
    REV_TRACE_MSG("playAction()");

    if (!player.play()) {
        REV_TRACE_MSG("player.play() FAILED");
    }
}

void QtSpectrumFilteringWindow::pauseAction() {
    REV_TRACE_MSG("pauseAction()");

    if (!player.pause()) {
        REV_TRACE_MSG("player.pause() FAILED");
    }
}

void QtSpectrumFilteringWindow::stopAction() {
    REV_TRACE_MSG("stopAction()");

    if (!player.stop()) {
        REV_TRACE_MSG("player.stop() FAILED");
    }
}

/**
 * @Override
 */
void QtSpectrumFilteringWindow::playEvent(rev::MP3Player* plr) {
    emit playSignal();
}

/**
 * @Override
 */
void QtSpectrumFilteringWindow::pauseEvent(rev::MP3Player* plr) {
    emit pauseSignal();
}

/**
 * @Override
 */
void QtSpectrumFilteringWindow::stopEvent(rev::MP3Player* plr) {
    emit stopSignal();
}

void QtSpectrumFilteringWindow::stopSlot() {
    REV_TRACE_MSG("stopEvent()");
    widget.playButton->setDisabled(false);
    widget.pauseButton->setDisabled(true);
    widget.stopButton->setDisabled(true);
}

void QtSpectrumFilteringWindow::pauseSlot() {
    REV_TRACE_MSG("pauseEvent()");
    widget.playButton->setDisabled(false);
    widget.pauseButton->setDisabled(true);
    widget.stopButton->setDisabled(false);
}

void QtSpectrumFilteringWindow::playSlot() {
    REV_TRACE_MSG("playEvent()");
    widget.playButton->setDisabled(true);
    widget.pauseButton->setDisabled(false);
    widget.stopButton->setDisabled(false);
}

void QtSpectrumFilteringWindow::repaintGraphsSlot() {
    signalAmplitudeGraph->repaint();
    signalSpectrumGraph->repaint();

    //widget.minFreqL->setText(QString::number(widget.minHzSlider->value()) + " Hz");
    //widget.maxFreqL->setText(QString::number(widget.maxHzSlider->value()) + " Hz");
}

QString QtSpectrumFilteringWindow::msToString(int millis) {
    int hours = millis / HOUR;
    millis -= hours * HOUR;
    int mins = millis / MINUTE;
    millis -= mins * MINUTE;
    int secs = millis / SECOND;
    millis -= secs * SECOND;

    QString time;
    time.reserve(16);

    if (gotHours || hours > 0) {
        if (hours < 10)
            time += "0";
        time += QString::number(hours);
        time += ":";
    }
    if (gotMinutes || mins > 0) {
        if (mins < 10)
            time += "0";
        time += QString::number(mins);
        time += ":";
    }

    if (secs < 10)
        time += "0";

    time += QString::number(secs);
    time += ".";

    if (millis < 10) {
        time += "00";
    } else if (millis < 100) {
        time += "0";
    }

    time += QString::number(millis);
    return time;
}

/**
 * @Override
 */
void QtSpectrumFilteringWindow::timeEvent(rev::MP3Player* plr,
        int currentSamplePos) {
    emit timeSignal(currentSamplePos);
}

void QtSpectrumFilteringWindow::timeSlot(unsigned int samplePos) {
    if (afterSeek) {
        afterSeek = false;
        return;
    }

    if (!sliderPressed) {
        widget.progressSlider->setValue(samplePos);
    }

    QString time = msToString((int) (msPerSample * samplePos));
    widget.currentTimeL->setText(time);
}

void QtSpectrumFilteringWindow::progressSliderMousePressedEvent(
        QMouseEvent* event) {
    sliderPressed = true;
}
void QtSpectrumFilteringWindow::progressSliderMouseReleasedEvent(
        QMouseEvent* event) {
    player.seek(sliderValue);
    afterSeek = true;
    sliderPressed = false;
}

void QtSpectrumFilteringWindow::gainValueChanged(int value) {
    float gain = 0.01f * value;
    signalSpectrumGraph->setGain(gain);
    widget.gainL->setText(QString::number(gain));

    //if (!player.isPlaying()) {
        emit repaintGraphsSignal();
    //}
}

void QtSpectrumFilteringWindow::maximalFrequencyValueChanged(int value) {
    widget.maxFreqL->setText(QString::number(value) + " Hz");

    signalSpectrumGraph->setMaxFrequency(value);

    if (!player.isPlaying()) {
        emit repaintGraphsSignal();
    }
}

void QtSpectrumFilteringWindow::minimalFrequencyValueChanged(int value) {
    widget.minFreqL->setText(QString::number(value) + " Hz");

    signalSpectrumGraph->setMinFrequency(value);

    if (!player.isPlaying()) {
        emit repaintGraphsSignal();
    }
}

void QtSpectrumFilteringWindow::progressSliderValueChangedEvent(int value) {
    if (!sliderPressed) {
        return;
    }
    sliderValue = value;

    QString time = msToString((int) (msPerSample * value));
    widget.currentTimeL->setText(time);

    if (!player.isPlaying()) {
        player.seek(sliderValue);
        afterSeek = true;
    }
}

