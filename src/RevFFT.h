/*
 * RevFFT.h
 *
 *  Created on: 20-10-2012
 *      Author: Revers
 */

#ifndef REVFFT_H_
#define REVFFT_H_

namespace rev {

    void fft(float* data, int nn);
    void ifft(float* data, int nn);

} /* namespace rev */
#endif /* REVFFT_H_ */
