/*
 * RevIAudioDataConsumer.h
 *
 *  Created on: 13-10-2012
 *      Author: Revers
 */

#ifndef REVIAUDIODATACONSUMER_HPP_
#define REVIAUDIODATACONSUMER_HPP_

#include <cstddef>
#include <rev/audio/RevAudioFormat.h>

namespace rev {

    class IAudioDataConsumer {
    public:

        virtual ~IAudioDataConsumer() {
        }

        virtual bool init(rev::AudioFormat format, int framesAtOnce) = 0;

        virtual void start() = 0;

        virtual void stop() = 0;

        virtual void flush() = 0;

        virtual bool isActive() = 0;

        virtual void consume(unsigned char* dataArray, int dataArrayLength, bool playing) = 0;


    };

}

#endif /* REVIAUDIODATACONSUMER_HPP_ */
