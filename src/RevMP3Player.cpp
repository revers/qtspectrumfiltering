/*
 * RevMP3Player.cpp
 *
 *  Created on: 09-10-2012
 *      Author: Revers
 */

#include <iostream>
#include <boost/thread.hpp>

#include <rev/audio/RevAudioFactory.h>
#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevTimer.h>
#include "RevMP3Player.h"

using namespace rev;
using namespace std;

#define REVMP3PLAYER_DEBUG_LINE __LINE__
#define REVMP3PLAYER_DEBUG_FUNCTION __PRETTY_FUNCTION__

#define debugState printState(REVMP3PLAYER_DEBUG_FUNCTION, \
        REVMP3PLAYER_DEBUG_LINE)

#define MP3PLAYER_ERR_MSG(X) rev::errout << func(REVMP3PLAYER_DEBUG_FUNCTION) << "," << __LINE__ << ": " << X << endl
#define DBG(X) cout << func(REVMP3PLAYER_DEBUG_FUNCTION) << "," << __LINE__ << ": " << X << endl

MP3Player::MP3Player(IAudioOutputStreamPtr outputStreamPtr) :
        consumer(outputStreamPtr) {
    pauseCondition = new boost::condition_variable_any();
    pauseMutex = new boost::mutex();
    seekMutex = new boost::mutex();
}

MP3Player::~MP3Player() {
    delete pauseMutex;
    delete seekMutex;
    delete pauseCondition;

    if (buffer) {
        delete[] buffer;
    }
}

bool MP3Player::open(const char* filename,
        int bufferSizeInFrames /*= 1024 */) {

    stop();
    consumer.stop();

    audioInputStream = rev::AudioFactory::createMP3FileInputStream(filename);

    if (audioInputStream == nullptr) {
        MP3PLAYER_ERR_MSG("ERROR: cannot create audio input stream!!");
        state = State::NOT_INITED;
        this->bufferSizeInFrames = 0;
        debugState;
        return false;
    }

    this->bufferSizeInFrames = bufferSizeInFrames;

    if (!consumer.init(audioInputStream->getFormat(), bufferSizeInFrames)) {
        MP3PLAYER_ERR_MSG("ERROR: cannot init audio data consumer!!");
        state = State::NOT_INITED;
        this->bufferSizeInFrames = 0;
        debugState;
        return false;
    } else {
        consumer.start();
    }

    bufferSizeInBytes = bufferSizeInFrames
            * audioInputStream->getFormat().getFrameSize();

    if (buffer) {
        delete[] buffer;
    }
    buffer = new unsigned char[bufferSizeInBytes];

    state = State::STOPPED;

    if (listener) {
        listener->stopEvent(this);
    }

    debugState;
    return true;
}

void MP3Player::run() {
    const float msPerSample = getMsPerSample();
    const int sampleSize = getFormat().getFrameSize();

    cout << a2s(R(msPerSample), R(sampleSize), R(bufferSizeInFrames)) << endl;

    int bytesRead;

    float msCounter = 0.0f;
    bool succ = true;

    while (state != State::STOPPED) {

        seekMutex->lock();
        succ = audioInputStream->read(buffer, bufferSizeInBytes, &bytesRead);
        if (!succ || bytesRead == 0) {
            seekMutex->unlock();
            break;
        }

        consumer.consume(buffer, bytesRead, true);

        seekMutex->unlock();

        float msRead = (float) (bytesRead / sampleSize) * msPerSample;
        msCounter += msRead;

        if (msCounter >= 100.0f) {
            msCounter -= 100.0f;

            if (state != State::SEEKING && listener) {
                listener->timeEvent(this, audioInputStream->getPosition());
            }
        }

        if (state == State::PAUSED) {
            DBG("pause -> wait");

            boost::unique_lock<boost::mutex> lock(*pauseMutex);
            pauseCondition->wait(*pauseMutex);
            DBG("after wait");
        }
    }

    if (!succ) {
        MP3PLAYER_ERR_MSG("ERROR: " << audioInputStream->getErrorMessage());
    } else {
        DBG("playing finished successfully :)");
        stop();
    }
}

bool MP3Player::play() {
    if (state != State::STOPPED && state != State::PAUSED) {
        debugState;
        return false;
    }

    if (!consumer.isActive()) {
        consumer.start();
    }

    State oldState = state;
    state = State::PLAYING;

    if (oldState == State::PAUSED) {
        DBG("notify_all()");
        boost::unique_lock<boost::mutex> lock(*pauseMutex);
        pauseCondition->notify_all();
    } else { // State::STOPPED
        boost::thread thread(&MP3Player::run, this);
    }

    debugState;

    if (listener) {
        listener->playEvent(this);
    }

    return true;
}

bool MP3Player::pause() {
    if (state != State::PLAYING) {
        debugState;
        return false;
    }

    state = State::PAUSED;
    debugState;

    if (listener) {
        listener->pauseEvent(this);
    }

    return true;
}

bool MP3Player::stop() {
    consumer.stop();

    if (state != State::PLAYING && state != State::PAUSED) {
        debugState;
        return false;
    }

    State oldState = state;
    state = State::STOPPED;

    if (oldState == State::PAUSED) {
        boost::unique_lock<boost::mutex> lock(*pauseMutex);
        pauseCondition->notify_all();
    }
    debugState;

    if (audioInputStream.get()) {
        audioInputStream->reset();

        if (listener) {
            listener->stopEvent(this);

            listener->timeEvent(this, audioInputStream->getPosition());
        }
    }

    return true;
}

void MP3Player::printState(const char* func, int line) {
    cout << func << ":" << line << ": state = ";
    switch (state) {
    case State::NOT_INITED:
        {
        cout << "NOT_INITED" << endl;
        break;
    }
    case State::PLAYING:
        {
        cout << "PLAYING" << endl;
        break;
    }
    case State::PAUSED:
        {
        cout << "PAUSED" << endl;
        break;
    }
    case State::STOPPED:
        {
        cout << "STOPPED" << endl;
        break;
    }
    case State::SEEKING:
        {
        cout << "SEEKING" << endl;
        break;
    }

    default:
        {
        cout << "UNKNOWN_STATE" << endl;
        break;
    }

    }
}

void MP3Player::seek(int samplePos) {
    if (state == State::NOT_INITED) {
        return;
    }
    seekMutex->lock();

    bool refreshData = false;
    if (state == State::PAUSED || state == State::STOPPED) {
        refreshData = true;
    }

    State lastState = state;
    state = State::SEEKING;
    debugState;

    if (!audioInputStream->seek(samplePos)) {
        MP3PLAYER_ERR_MSG("ERROR: " << audioInputStream->getErrorMessage());
    }

    state = lastState;
    debugState;

    if (refreshData) {
        int bytesRead;
        bool succ = audioInputStream->read(buffer, bufferSizeInBytes, &bytesRead);
        if (!succ || bytesRead == 0) {
            MP3PLAYER_ERR_MSG("ERROR: " << audioInputStream->getErrorMessage());
        }

        consumer.consume(buffer, bytesRead, false);
    }

    seekMutex->unlock();
}

