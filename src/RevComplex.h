/*
 * RevComplex.h
 *
 *  Created on: 20-10-2012
 *      Author: Revers
 */

#ifndef REVCOMPLEX_H_
#define REVCOMPLEX_H_

namespace rev {

    struct complex {
        float re;
        float im;

        complex() :
                re(0), im(0) {
        }
        complex(float re, float im) :
                re(re), im(im) {
        }
    };

    inline complex operator+(const complex& a, const complex& b) {
        return complex(a.re + b.re, a.im + b.im);
    }

    inline complex& operator+=(complex& a, const complex& b) {
        a.re += b.re;
        a.im += b.im;
        return a;
    }

    inline complex operator-(const complex& a, const complex& b) {
        return complex(a.re - b.re, a.im - b.im);
    }

    inline complex& operator-=(complex& a, const complex& b) {
        a.re -= b.re;
        a.im -= b.im;
        return a;
    }

    inline complex operator/(const complex& a, const float& x) {
        return complex(a.re / x, a.im / x);
    }

    inline complex& operator/=(complex& a, const float& x) {
        a.re /= x;
        a.im /= x;
        return a;
    }

    inline complex operator*(const complex& a, const complex& b) {
        return complex(a.re * b.re - a.im * b.im, a.re * b.im + a.im * b.re);
    }

    inline complex operator*(const complex& a, const float& x) {
        return complex(a.re * x, a.im * x);
    }

    inline complex& operator*=(complex& a, const float& x) {
        a.re *= x;
        a.im *= x;
        return a;
    }
}
#endif /* REVCOMPLEX_H_ */
