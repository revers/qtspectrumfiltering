/*
 * RevIPlayerListener.h
 *
 *  Created on: 09-10-2012
 *      Author: Revers
 */

#ifndef REVIPLAYERLISTENER_HPP_
#define REVIPLAYERLISTENER_HPP_

#include <cstddef>

namespace rev {

    class MP3Player;

    class IPlayerListener {
    public:
        ~IPlayerListener() {
        }

        virtual void playEvent(rev::MP3Player* plr) = 0;

        virtual void pauseEvent(rev::MP3Player* plr) = 0;

        virtual void stopEvent(rev::MP3Player* plr) = 0;

        /**
         * While playing, notifies every 100ms about current position in samples.
         * If stream has no beginning and no end "currentSamplePos" equals always 0.
         */
        virtual void timeEvent(rev::MP3Player* plr, int currentSamplePos) = 0;
    };
}

#endif /* REVIPLAYERLISTENER_HPP_ */
